<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'products', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->bigInteger( 'user_id' );
			$table->string( 'name' );

		});
    }	

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
        Schema::dropIfExists( 'products', function( Blueprint $table ){});
    }

}