<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stock', function( Blueprint $table ) {
                    $table->bigIncrements('id');
                    $table->bigInteger('product_id');
                    $table->bigInteger('category_id');
                    $table->string('code');
                    $table->string('title');
                    $table->string('image');
                    $table->text('color');
                    $table->text('size');
                    $table->bigInteger('quantity');
                    $table->decimal('unit_price', 15, 2);
                    $table->text('description');
                    $table->string('other');
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('stock', function(Blueprint $table) {
                    //
        });
	}

}