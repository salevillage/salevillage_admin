<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'order', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->bigInteger( 'user_id' );
			$table->integer( 'status' );
			$table->date('created_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists( 'order', function( Blueprint $table ){}); 
	}

}