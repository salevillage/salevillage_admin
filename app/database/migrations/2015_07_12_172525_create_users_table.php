<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'users', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->string( 'email' );
			$table->string( 'password' );
			$table->string( 'name' );
			$table->integer( 'user_role' );
			$table->date( 'last_login' );
			$table->integer( 'status' );
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists( 'users', function( Blueprint $table ){});
	}

}