<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'stock_order', function( Blueprint $table ){
			$table->bigIncrements('id');
			$table->bigInteger('stock_id');
			$table->bigInteger('order_id');
			$table->integer('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists( 'stock_order', function( Blueprint $table ){});
	}

}
?>