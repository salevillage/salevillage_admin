<?php

class PageController extends BaseController {

  protected $userproducts;

  public function __construct( Userproducts $userproducts ) {
    parent::__construct();

    $this->userproducts = $userproducts;
  }

  public function showHome() {
    $product = new Product;
    $getproducts = $product->get();
    return View::make( 'page.home' )->with( array( 'products' => $getproducts ) );
  }

  public function showSingleProduct( $shopid, $code ) {
    $getproduct = Product::where( 'code', '=', $code )->get();
    $relatedproducts = Product::where( 'shop_id', '=', $shopid )->where( 'code', '<>', $code )->get();
    return View::make( 'page.product-detail' )->with( array( 'product' => $getproduct[0], 'relatedproducts' => $relatedproducts ) );
  }

  public function showSingleCategory( $name ) {
    $getcategories = Category::where( 'name', '=', $name )->get();
    return View::make( 'list.productscategory' )->with( array( 'categories' => $getcategories ) );
  }

  public function showAbout() {
    return View::make( 'page.aboutus' );
  }

  public function showTermsandconditions() {
    return View::make( 'page.termsandconditions' );
  }

  public function showLoveList() {
    $user = Auth::user();
    $loveproducts = User::find( $user->id )->loveproducts;
    return View::make( 'list.love' )->with( array( 'loveproducts' => $loveproducts ) );
  }

  public function doAddToCart() {
    if ( Session::has( 'product' ) ) {
      $sess_products = Session::get( 'product' );
      foreach ( $sess_products as $pro ) {
        $new[] = array(
          'id' => $pro['id'],
          'color_id' => $pro['color_id'],
          'size_id' => $pro['size_id'],
        );
      }
      $exist = in_array( array( 'id' => $_GET['id'], 'color_id' => ( isset( $_GET['color'] ) ? $_GET['color'] : 0 ), 'size_id' => ( isset( $_GET['size'] ) ? $_GET['size'] : 0 ) ), $new );
      if ( $exist == true ) {
        $result = array(
          'success' => false,
          'status' => 'You already added this item!',
          'count' => count( $sess_products ),
        );
      } else {

        $this->addproducttosession( $_GET['id'], ( isset( $_GET['color'] ) ? $_GET['color'] : 0 ), ( isset( $_GET['size'] ) ? $_GET['size'] : 0 ) );
        $result = array(
          'success' => true,
          'count' => count( Session::get( 'product' ) ),
        );
      }
    } else {
      $this->addproducttosession( $_GET['id'], ( isset( $_GET['color'] ) ? $_GET['color'] : 0 ), ( isset( $_GET['size'] ) ? $_GET['size'] : 0 ) );
      $result = array(
        'success' => true,
        'count' => 1,
      );
    }
    Session::put( 'cart_count', count( Session::get( 'product' ) ) );
    echo json_encode( $result );
  }

  public function addproducttosession( $id, $color, $size ) {
    $product = array(
      'id' => $id,
      'color_id' => $color,
      'size_id' => $size,
      'quantity' => 1,
    );
    if ( Session::push( 'product', $product ) )
      return true;
    else
      return false;
  }

  /*
     * public function doAddToCart() {
      $product = array(
      'id' => $_GET['id'],
      'size_id' => (isset($_GET['size']) ? $_GET['size'] : 0),
      'color_id' => (isset($_GET['color']) ? $_GET['color'] : 0),
      );
      Session::push('product', $product);
      $sess_products = Session::get('product');

      if (is_null($sess_products)) {
      $result = array(
      'success' => false,
      );
      } else {
      $result = array(
      'success' => true,
      'count' => count($sess_products),
      );
      }

      return json_encode($result);
      }
     */

  public function showCartList() {
    if ( isset( $_GET['productid'] ) ) {          //for ajax to delete the data from session by productid
      $sess_products = Session::get( 'product' );   //get the products from session product
      foreach ( $sess_products as $key => $product ) {
        if ( ( $product['id'] == $_GET['productid'] ) && ( $product['color_id'] == $_GET['color'] ) && ( $product['size_id'] == $_GET['size'] ) ) {
          Session::forget( 'product.' . $key );
        }
      }
      Session::put( 'cart_count', count( Session::get( 'product' ) ) );
      $ret = array(
        'success' => true,
      );
      echo json_encode( $ret );
    } else {                                  //add the product data to session and show cart-list page
      $value = Session::get( 'product' );
      if ( isset( $value ) ) {
        foreach ( $value as $val ) {
          foreach ( $val as $key => $v ) {
            if ( $key == 'id' ) {
              $products[] = Product::find( $v );
            }
          }
        }
        if ( !empty( $products ) ) {
          return View::make( 'list.cart' )->with( array( 'products' => $products ) );
        } else { // after removing all add to cart items
          Session::forget( 'product' );
          return View::make( 'list.cart' )->with( array( 'message' => "You didn't add any product" ) );
        }
      } else {
        return View::make( 'list.cart' )->with( array( 'message' => "You didn't add any product" ) );
      }
    }
  }

  public function checkout() {
    if ( Auth::check() ) {     //already login
      /**
       * Add quantity and amount to products array to pass checkout page and email page
       */
      $qtybyproducts = array_map( 'current', Input::get( 'product' ) );
      $sess_products = Session::get( 'product' );
      if ( isset( $sess_products ) ) {
        foreach ( $sess_products as $seskey => $pro ) {
          foreach ( $qtybyproducts as $key => $qty ) {
            if ( $seskey == $key ) {
              if ( $qty == '' ) {
                $product = Product::find( $pro['id'] );
                $amount = $product->price;
              } else {
                $product = Product::find( $pro['id'] );
                $amount = $product->price * $qty;
              }

              $products[] = array(
                'id' => $pro['id'],
                'size_id' => $pro['size_id'],
                'color_id' => $pro['color_id'],
                'price' => $product->price,
                'quantity' => ( $qty == '' ? 1 : $qty ),
                'amount' => $amount,
              );
            }
          }
        }

        /**
         * Add to userproducts table
         */
        if ( isset( $products ) ) {
          foreach ( $products as $product ) {
            $userproducts = $this->userproducts->create( array(
                'product_id' => $product['id'],
                'user_id' => Auth::user()->id,
                'size_id' => $product['size_id'],
                'color_id' => $product['color_id'],
                'quantity' => $product['quantity'],
              ) );
          }

          if ( $userproducts ) {
            /**
             * Send Mail to user
             */
            $data = array( 'products' => $products );
            Mail::send( 'page.email', $data, function( $m ) {
                $m->to( 'thandar@evolxit.com', 'Baxket' );
                $m->subject( 'Welcome to Baxket' );
              } );
            Session::forget( 'product' );   //after check out clear product session
            Session::forget( 'cart_count' );   //after check out clear product session
            return View::make( 'page.checkout' )->with( array( 'message' => "All of your products are check out and check your e-mail.", 'products' => $products ) );
          }
        }
      }
    } else {        //not login
      return View::make( 'list.cart' )->with( array( 'message' => "You didn't login" ) );
    }
  }

  public function showSearch( $name ) {
    if ( !empty( $name ) ) {
      $searchTerms = explode( ' ', $name );

      foreach ( $searchTerms as $term ) {
        $query = Product::where( 'name', 'LIKE', '%' . $term . '%' );
      }
      $product = $query->get();

      $products = $query->get()->toArray();

      if ( !empty( $products ) ) {
        return View::make( 'page.search' )->with( array( 'products' => $product, 'search_name' => $name ) );
      } else {
        return View::make( 'page.search' )->with( array( 'message' => 'No results were found for your search - ' . $name ) );
      }
    }
  }

  /*
     * * Save Store & Save Prodcut
     */

  public function doCreateStore() {
    $facebook = new Facebook( Config::get( 'facebook' ) );
    if ( isset( $_POST['store_name'] ) && !empty( $_POST['store_name'] ) ) {
      $fbpageids = $_POST['store_name'];
     
      foreach ( $fbpageids as $fbpageid ) {
        /*
        ** Save Store
        */
        $page_info = $facebook->api( '/' . $fbpageid );
        
        if ( !empty( $page_info ) ) {
          $store = Store::create( array(
              'page_id' => isset( $page_info['id'] ) ? $page_info['id'] : '',
              'username' => isset( $page_info['username'] ) ? $page_info['username'] : '',
              'name' => isset( $page_info['name'] ) ? $page_info['name'] : '',
              'link' => isset( $page_info['link'] ) ? $page_info['link'] : '',
              'likes' => isset( $page_info['likes'] ) ? $page_info['likes'] : '',
              'website' => isset( $page_info['website'] ) ? $page_info['website'] : '',
              'category' => isset( $page_info['category'] ) ? $page_info['category'] : '',
              'phone' => isset( $page_info['phone'] ) ? $page_info['phone'] : '',
              'email' => isset( $page_info['email'] ) ? $page_info['email'] : '',
              'address' => isset( $page_info['address'] ) ? $page_info['address'] : '',
            ) );
        }

        /*
        ** Save products from Facebook Pages
        */
        $products = $facebook->api( '/'.$fbpageid.'/posts?limit=5' );  // Select Latest Post Limit
//         parent::pr($products);exit;
        if ( isset( $products ) && ( !empty( $products ) ) ) {
          foreach ( $products['data'] as $product['data'] ) {
            $name = isset( $product['data']['name'] ) ? $product['data']['name'] : '';
//            $shop_id = isset( $product['data']['from']['id'] ) ? $product['data']['from']['id'] : '';
            $shop_id = $store->id;
            $image = isset( $product['data']['picture'] ) ? $product['data']['picture'] : '';
            $message = isset( $product['data']['message'] ) ? $product['data']['message'] : '';
            $capation = isset( $product['data']['capation'] ) ? $product['data']['capation'] : '';
            $product_fblink = isset($product['data']['product_fblink'] ) ? $product['data']['product_fblink'] : '';
            $description = isset( $product['data']['description'] ) ? $product['data']['description'] : '';
            parent::pr(strpos($image, '_s.'));exit;
            if( !empty($image) ){
              if( strpos($image, '_s.') == true){
                $original_img_size = (str_replace("_s.","_o.",$image));
              }
            }
            if ( ( !empty( $name ) ) || ( !empty( $message ) ) || ( !empty( $capation ) ) || ( !empty( $description ) ) ) {
              Product::create( array(
                  'name' => $name,
                  'shop_id' => $shop_id,
                  'code' => str_random(6),
                  'image' => $original_img_size,
                  'description' => $message . ' ' . $capation . ' ' . $description,
                  'product_fblink' => $product_fblink,
                ) );
            }
          }
        }
      }  /*endforeach*/

    } else {
      $user_id = $facebook->getUser();
      $useraccounts = $facebook->api( '/' . $user_id . '/accounts' );
      if ( !empty( $useraccounts['data'] ) ) {
        return View::make( 'list.fbpages' )->with( array( 'error_message' => "Please select any page!", 'fbuserpages' => $useraccounts['data'] ) );
      }
    }
    $params = array(
      'redirect_uri' => url( '/signup/fb/callback' ),
      'scope' => 'email, manage_pages',
    );
    return Redirect::to( '/' )->with( 'message', 'Logged in with Facebook' );
  }

  // public function doProductCreate(  ) {
  //     $fb = new Facebook(Config::get('facebook'));

  //     if (!empty($fb)) {
  //         $products = $fb->api( '/'.$page_id.'/posts' );
  //         if (isset($products) && (!empty($products) )) {
  //             for ($i = 0; $i < 2; $i++) {
  //                 $name = isset($products['data'][$i]['name']) ? $products['data'][$i]['name'] : '';
  //                 $shop_id = isset($products['data'][$i]['from']['id']) ? $products['data'][$i]['from']['id'] : '';
  //                 $image = isset($products['data'][$i]['picture']) ? $products['data'][$i]['picture'] : '';
  //                 $message = isset($products['data'][$i]['message']) ? $products['data'][$i]['message'] : '';
  //                 $capation = isset($products['data'][$i]['capation']) ? $products['data'][$i]['capation'] : '';
  //                 $description = isset($products['data'][$i]['description']) ? $products['data'][$i]['description'] : '';
  //                 if ((!empty($name) ) || (!empty($message) ) || (!empty($capation) ) || (!empty($description) )) {
  //                     Product::create(array(
  //                         'name' => $name,
  //                         'shop_id' => $shop_id,
  //                         'image' => $image,
  //                         'description' => $message . ' ' . $capation . ' ' . $description,
  //                     ));
  //                 }
  //             }
  //         }
  //         Session::flash('success', 'Successfully Fetched Items.');
  //         return Redirect::to('shops');
  //     } else {
  //         return Redirect::to('shops');
  //     }
  // }

  public function doLoginInFb() {

    $facebook = new Facebook( Config::get( 'facebook' ) );
    $params = array(
      'redirect_uri' => url( '/login/fb/callback' ),
      'scope' => 'email',
    );
    return Redirect::to( $facebook->getLoginUrl( $params ) );
  }

  public function doLoginfbcallback() {
    $code = Input::get( 'code' );
    if ( strlen( $code ) == 0 )
      return Redirect::to( '/' )->with( 'message', 'There was an error communicating with Facebook' );

    $facebook = new Facebook( Config::get( 'facebook' ) );
    $uid = $facebook->getUser();

    if ( $uid == 0 )
      return Redirect::to( '/' )->with( 'message', 'There was an error' );

    $me = $facebook->api( '/me' );

    $profile = Profile::whereUid( $uid )->first();
    if ( empty( $profile ) ) {

      $user = new User;
      $user->name = $me['first_name'] . ' ' . $me['last_name'];
      $user->email = $me['email'];
      $user->phone = 'https://graph.facebook.com/' . $me['username'] . '/picture?type=large';
      $user->save();

      $profile = new Profile();
      $profile->uid = $uid;
      $profile->username = $me['username'];
      $prof = $user->profiles()->save( $profile );
    }

    $profile->access_token = $facebook->getAccessToken();
    $profile->save();
    $user = $prof->user;

    Auth::login( $user );
    return Redirect::to( '/' )->with( 'message', 'Logged in with Facebook' );
  }

  public function doSignUpStore() {
    $facebook = new Facebook( Config::get( 'facebook' ) );
    $params = array(
      'redirect_uri' => url( '/signup/fb/callback' ),
      'scope' => 'email, manage_pages',
    );
//    parent::pr($params);exit;
    return Redirect::to( $facebook->getLoginUrl( $params ) );
  }

  public function doSignUpfbcallback() {
    $code = Input::get( 'code' );
    if ( strlen( $code ) == 0 )
      return Redirect::to( '/' )->with( 'message', 'There was an error communicating with Facebook' );
    $facebook = new Facebook( Config::get( 'facebook' ) );
    $uid = $facebook->getUser();
    if ( $uid == 0 )
      return Redirect::to( '/' )->with( 'message', 'There was an error' );
    $me = $facebook->api( '/me' );
    $profile = Profile::whereUid( $uid )->first();
    if ( empty( $profile ) ) {
      $user = new User;
      $user->name = $me['first_name'] . ' ' . $me['last_name'];
      $user->email = $me['email'];
      $user->phone = 'https://graph.facebook.com/' . $me['username'] . '/picture?type=large';
      $user->save();

      $profile = new Profile();
      $profile->uid = $uid;
      $profile->username = $me['username'];
      $profile = $user->profiles()->save( $profile );
    }

    $profile->access_token = $facebook->getAccessToken();
    $profile->save();
    $user = $profile->user;
    Auth::login( $user );
    /*
     ** Facebook pages
     */
    $user_id = $facebook->getUser();
    $ret = $facebook->api( '/' . $user_id . '/accounts' );
//    parent::pr($ret['data']);exit;
    if ( !empty( $ret['data'] ) ) {
      return View::make( 'list.fbpages' )->with( array( 'fbuserpages' => $ret['data'] ) );
    } else {
      return View::make( 'list.fbpages' )->with( array( 'message' => "You don't have any page!" ) );
    }
  }

}
