<?php
class CategoryController extends BaseController{
  public function index(){
    $cats = Category::all();
    return View::make('category.cat_view')->with('cats',$cats);
  }
  
  public function viewcat(){    
    $cats = Category::all();
    return View::make('category.cat_view')->with('cats',$cats);
  }
  
  public function showcat($id){
    $cats = Category::find($id);
    return View::make('category.cat_show')->with('cats',$cats);
  }
  
  public function createcat(){
    if(Input::has('title')){
      $cat = Category::addData(Input::get('title'));
      if($cat == 1){
        return Redirect::to('viewcat');  
      }
    }else{
      return View::make('category.cat_create');
    }    
  }
  
  public function editcat($id){
    $cat = Category::find($id);
    return View::make('category.cat_edit')->with('cat',$cat);
  }
  
  public function update($id){
    $rules = array(
        'title'=>'required',
   );
    $validator = Validator::make(Input::all(),$rules);
    
    if($validator->fails()){
      return Redirect::to('category/'.$id.'/edit')->withErrors($validator); 
    }else{
      $cat = Category::find($id);
      $cat->title = Input::get('title');
      $cat->save();      
//      Session::flash('message', 'Successfully updated post!');
      return Redirect::to('category');
    }
  }
  
  public function deletecat($id){
    $cat = Category::find($id);
    $cat->delete();
    return Redirect::to('category');
  }
}
?>
