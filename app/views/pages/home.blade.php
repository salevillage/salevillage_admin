@extends('layouts.dashboard')
@section('content')
	<!DOCTYPE html>

			<div class="row dashboard-bg">
				<div class="col-md-12 dashboard">
					<a class="navbar-brand" href="#">Dashboard</a>
				</div>
			</div>
			<div class="row status-info">
				<div class="col-md-3">
					<div class="order">
						<h3>Order</h3>
						<span id="order-item-count">6</span>
					</div>
				</div>
				<div class="col-md-5">
					<div class="sales">
						<h3>Sales</h3>
						<span id="sale-amt">$3456</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tax">
						<h3>Sales</h3>
						<span id="tax-amt">$34</span>
					</div>
				</div>
			</div>
			<div class="row main-content">
				<div class="col-md-6">
					<div class="recent-orders">
						<h2>Recent Orders</h2>
						<ul class="nav nav-pills">
						  <li role="presentation" class="active"><a href="#">All</a></li>
						  <li role="presentation"><a href="#">Cancel</a></li>
						  <li role="presentation"><a href="#">Pending</a></li>
						  <li role="presentation"><a href="#">Complete</a></li>
						</ul>
						<div class="order-detail">
							<ul class="order-label">
								<li><a href="#"><span class="label label-default">Cancel</span></a></li>
								<li>Order#97 by malvic</li>
								<li>mm/dd/yy</li>
								<li>Amount</li>
							</ul>
							<ul class="order-label">
								<li><a href="#"><span class="label label-success">Success</span></a></li>
								<li>Order#97 by malvic</li>
								<li>mm/dd/yy</li>
								<li>Amount</li>
							</ul>
							<ul class="order-label">
								<li><a href="#"><span class="label label-info">Pending</span></a></li>
								<li>Order#97 by malvic</li>
								<li>mm/dd/yy</li>
								<li>Amount</li>
							</ul>
							<ul class="order-label">
								<li><a href="#"><span class="label label-info">Pending</span></a></li>
								<li>Order#97 by malvic</li>
								<li>mm/dd/yy</li>
								<li>Amount</li>
							</ul>
							<ul class="order-label">
								<li><a href="#"><span class="label label-info">Pending</span></a></li>
								<li>Order#97 by malvic</li>
								<li>mm/dd/yy</li>
								<li>Amount</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="statics">
						
					</div>
				</div>
			</div>
			<div class="row active-record">
				<div class="col-md-2">
					<div class="panel panel-default">
					  <div class="panel-heading">Active Products</div>
					  <div class="panel-body">
					    <span>200</span>
					  </div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="panel panel-default">
					  <div class="panel-heading">Out of Stocks</div>
					  <div class="panel-body">
					    <span>20</span>
					  </div>
					</div>

				</div>
				<div class="col-md-2">
					<div class="panel panel-default">
					  <div class="panel-heading">In Stocks</div>
					  <div class="panel-body">
					    <span>100</span>
					  </div>
					</div>

				</div>
				<div class="col-md-3">
					<div class="panel panel-default">
					  <div class="panel-heading">Registered Users</div>
					  <div class="panel-body">
					    <span>2</span>
					  </div>
					</div>

				</div>
				<div class="col-md-2">
					<div class="panel panel-default">
					  <div class="panel-heading">Categories</div>
					  <div class="panel-body">
					    <span>3</span>
					  </div>
					</div>

				</div>
				<div class="col-md-1">
					<div class="panel panel-default">
					  <div class="panel-heading">Stores</div>
					  <div class="panel-body">
					    <span>1</span>
					  </div>
					</div>

				</div>
			</div>
</div>
</div>
@stop