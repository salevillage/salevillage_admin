<html>
  <head><title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    {{ HTML::style('css/custom.css'); }}
  </head>
  <div class="container">
  <body>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <td>ID</td>
          <td>Title</td>
          <td></td>
        </tr>
      </thead>
      
      <nav class="navbar navbar-inverse">
        <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('category') }}">View All Categories</a>
        </div>
        <ul class="nav navbar-nav">
          <li><a href="{{ URL::to('category') }}">View All Categories</a></li>
          <li><a href="{{ URL::to('createcat') }}">Create a Category</a>
        </ul>
      </nav>
        <tbody>
      @foreach($cats as $key => $value)
        <tr>
          <td>{{ $value->id }}</td>
          <td>{{ $value->title }}</td>

          <!-- we will also add show, edit, and delete buttons -->
          <td>

             <!--delete the post (uses the destroy method DESTROY /post/{id}--> 
             <!--we will add this later since its a little more complicated than the other two buttons--> 
             <!--show the post (uses the show method found at GET /post/{id}-->
            <a class="btn btn-small btn-success" href="{{ URL::to('showcat/' . $value->id) }}">Show this Category</a>

             <!--edit this post (uses the edit method found at GET /post/{id}/edit--> 
            <a class="btn btn-small btn-info" href="{{ URL::to('viewcat/' . $value->id .'/edit') }}">Edit this Category</a>
            
            <!--delete this post-->
            <a class="btn btn-small btn-delete" href="{{ URL::to('deletecat/' . $value->id) }}">Delete this Category</a>

          </td>
        </tr>
      @endforeach
      </tbody>
      </table>
      
  </body>
  </div>
</html>

