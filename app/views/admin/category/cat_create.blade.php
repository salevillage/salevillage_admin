<html>
  <head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-inverse">
        <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('category') }}">View All Categories</a>
        </div>
        <ul class="nav navbar-nav">
          <li><a href="{{ URL::to('category') }}">View All Categories</a></li>
          <li><a href="{{ URL::to('catcreate') }}">Create a Category</a>
        </ul>
      </nav>
      <h1>Create a Category</h1>
      {{ Form::open(array('url' => 'createcat')) }}
      <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
      </div>
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
      {{ Form::close() }}
    </div>
  </body>
</html>
