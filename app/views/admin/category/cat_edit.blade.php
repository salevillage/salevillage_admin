<html>
  <head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-inverse">
        <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('post') }}">View All Categories</a>
        </div>
        <ul class="nav navbar-nav">
          <li><a href="{{ URL::to('category') }}">View All Categories</a></li>
          <li><a href="{{ URL::to('createcat') }}">Create a Category</a>
        </ul>
      </nav>
      {{ HTML::ul($errors->all()) }}
      <!--{{ Form::model($cat, array('route' => array('category.update', $cat->id), 'method' => 'POST')) }}-->
      <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
      </div>

      {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
    </div>  
  </body>
</html>