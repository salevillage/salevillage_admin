<html>
<head>
	<title>Look! I'm CRUDding</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  <?php HTML::style('css/custom.css');?>
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
	<a class="navbar-brand" href="{{ URL::to('post') }}">View All Posts</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('category') }}">View All Categories</a></li>
		<li><a href="{{ URL::to('createcat') }}">Create a Category</a>
	</ul>
</nav>

<h1>Showing Category title "{{$cats->title}}"</h1>

	<div class="jumbotron text-center">
    <p>
      <strong>Title : {{$cats->title}}</strong>
    </p>
	</div>

</div>
</body>
</html>