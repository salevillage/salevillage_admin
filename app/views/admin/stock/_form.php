<!doctype html>
<html>
<head>
	@include('views.admin.stock._form')
</head>
<body>
<div class="container">
	<div id="main" class="row">
			@yield('content')
	</div>

	<footer class="row">
		@include('includes.footer')
	</footer>
</div>
</body>
</html>