<html>
<head>
	<title>SaleVillage</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/salvg.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
</head>
<body>
<div class="main" id="home">
	<div class="container">
		<div class="row">
			<div class="col-md-12 header-brand">
					<a class="navbar-brand" >SaleVillage</a>
					
					<ul class="user-loggin">
						 <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i><span class="caret"></span></a>
								          <ul class="dropdown-menu user-loggin-drop">
								            <li><a href="#">Edit Profile</a></li>
								            <li role="separator" class="divider"></li>
								            <li><a href="#">Sign Out</a></li>
								          </ul>
					      </li>
					</ul>
			</div>
		</div><!--row-->
			<div class="row header-menu">
				<div class="col-md-8 main-menu">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<div class="navbar-header">
							 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						        <span class="sr-only">Toggle navigation</span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						      </button>
						      <a class="navbar-brand" href="#"></a>
												
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								      <ul class="nav navbar-nav">
								      	<li>
								      		<a href="/"><i class="fa fa-home"></i></a>
								      	</li>
								        <li><a href="#">Orders <span class="sr-only">(current)</span></a></li>
								        <li><a href="#">Stores</a></li>
								        <li class="dropdown">
								          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users <span class="caret"></span></a>
								          <ul class="dropdown-menu">
								         
								        
								            <li><a href="#">Administrators</a></li>
								            <li role="separator" class="divider"></li>
								            <li><a href="#">Guests</a></li>
								          </ul>
					        			</li>
								        <li class="dropdown">
								          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products <span class="caret"></span></a>
								          <ul class="dropdown-menu">
								            <li><a href="categories.html">Categories</a></li>
								            <li role="separator" class="divider"></li>
								            <li><a href="products.html">Products</a></li>
								          </ul>
					        			</li>
					      			</ul>
								    
								     
					    </div><!-- /.navbar-collapse -->					
					</div>
				</nav>
				</div>
					<div class="col-md-4">
				  <form class="navbar-form navbar-left" role="search">
								        <div class="form-group">
								          <input type="text" class="form-control" placeholder="Search">
								        </div>
								        <button type="submit" class="btn btn-default">Submit</button>
								      </form>
			</div>
			</div>