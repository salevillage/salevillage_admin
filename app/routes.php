<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('pages.home');
});
Route::get('about', function()
{
	return View::make('pages.about');
});
Route::get('contact', function()
{
	return View::make('pages.contact');
});

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function() {

            View::share(array(
                'loggedin' => Auth::user(),
                'success' => Session::get('success'),
            ));
            

            Route::group(array('prefix' => 'stock'), function() {
                Route::get(
                        '/{id}', array('uses' => 'StockController@showSingle')
                )->where('id', '[0-9]+');
                //show product create form
                Route::get(
                        '/create', array('uses' => 'StockController@showCreate')
                );
                //save product after create
                Route::post(
                        '/create', array('uses' => 'StockController@doCreate')
                );
                // show product update form
                Route::get(
                        '/update/{id}', array('uses' => 'StockController@showUpdate')
                )->where('id', '[0-9]+');
                // save product after update
                Route::post(
                        '/update/{id}', array('uses' => 'StockController@doUpdate')
                )->where('id', '[0-9]+');
                // delete product
                Route::get(
                        '/delete/{id}', array('uses' => 'StockController@doDelete')
                )->where('id', '[0-9]+');
            });

            	Route::group(array('prefix' => 'user'), function() {
                        // show single user
                        Route::get(
                                '/{id}', array('uses' => 'UserController@showSingle')
                        )->where('id', '[0-9]+');
                        // show user create form
                        Route::get(
                                '/create', array('uses' => 'UserController@showCreate')
                        );
                        // save user after create
                        Route::post(
                                '/create', array('uses' => 'UserController@doCreate')
                        );

                        // show user update form
                        Route::get(
                                '/update/{id}', array('uses' => 'UserController@showUpdate')
                        )->where('id', '[0-9]+');
                        // save user after update
                        Route::post(
                                '/update/{id}', array('uses' => 'UserController@doUpdate')
                        )->where('id', '[0-9]+');
                        // delete user
                        Route::get(
                                '/delete/{id}', array('uses' => 'UserController@doDelete')
                        )->where('id', '[0-9]+');
                    });
        });






